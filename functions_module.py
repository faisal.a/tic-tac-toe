#pythons built in library needed for generating a random number (used later)
import random

# Note about the reason why there are two files instead of just one.
# Mostly for ease of use, cause one contains all the methods that are gonna be used and 
# the other file is calling these methods for the program in the right sequence

# Using a dictionary to make a model of the board because it has a form of:
#				{key:value,key:value}
# where keys are used to get the value and the value can be changed to anything but the
# keys don't change which is good for the model cause that would mean we can't access
# a specific position on the board anymore
board_map= {1:1,2:2,3:3,4:4,5:5,6:6,7:7,8:8,9:9}

# used only in the initial version, not used anymore, only here to keep 
# bugs from happening when it's called somewhere in the program cause 
# im too lazy to remove them all
state_number = 1
# this var is used for knowing whether someone one the game or not
# 0 = False, 1 = True
# same convention is followed in the whole program
won_state = 0
#global variable for knowing when does the player win, so shit isn't printed twice
player_win = 0
#same for computer
computer_win = 0

#make a board
def make_board():
	#this skips a line, makes the table look better
	print("\n")
	# the .format is a python's built in method that is used here
	# it works like this
	# print("{}".format(Your input))
	# the input goes in the {}
	# a quick google search can explain its usage in an elaborate way
	r1 = " 			 {}  |  {}  | {} "
	print(r1.format(board_map[1],board_map[2],board_map[3]))
	print("			--------------")
	r2 = "			 {}  |  {}  | {} "
	print(r2.format(board_map[4],board_map[5],board_map[6]))
	print("			--------------")
	r3 = " 			 {}  |  {}  | {} "
	print(r3.format(board_map[7],board_map[8],board_map[9]))
	
#take the player input
def player_input():
	print("\n")
	user_var = input("Please type the coordinate for your move ")
	try:
		if is_available(user_var):
			if KeyError == False:
				print("Are you retarded?")
	except KeyError:
		global board_map
		board_map[int(user_var)] = "X"
		# print(board_map)
		make_board()
		global state_number
		state_number += 1

	# if is_available(user_var) == True:
	# 	global board_map
	# 	board_map[int(user_var)] = "X"
	# 	# print(board_map)
	# 	make_board()
	# 	global state_number
	# 	state_number += 1
	# elif is_available(user_var) == False:
	# 	print("Are you retarded?")


def computer_game_input(moves):
	global board_map
	board_map[int(moves)] = "0"
	# print(board_map)

def computer_game_input_random(moves):
	if is_available(moves) == True:
		# print("Its me")
		global board_map
		board_map[int(moves)] = "0"

	else:
		choose_random()

def choose_random():
	if state_number!=0:
		# print("hello")
		global board_map
		temp = list(board_map)
		# print("WJIOWNDIAN")
		# print("THIS IS TEMP {}".format(temp))
		temp_1 = []
		for i in range(1,9):
			if type(temp[i]) == int:
				temp_1.append(i) 
		# print(temp_1)
		computer_game_input_random(random.choice(temp_1))

def is_available(number):
	# print(board_map[number])
	if board_map[number] == '0' or board_map[number] == 'X':
		return False
	else:
		return True

def player_won(number_1,number_2,number_3):
	global player_win
	if board_map[number_1] == 'X' and board_map[number_2] == 'X' and board_map[number_3] == 'X' and player_win == 0:
		return True
		global won_state
		won_state +=1
		player_win +=1
		# print("won state is {}".format(won_state))
	else:
		return False
def computer_won(number_1,number_2,number_3):
	global computer_win
	if board_map[number_1] == '0' and board_map[number_2] == '0' and board_map[number_3] == '0' and computer_win == 0:
		print("Your future overlord won")
		global won_state
		won_state +=1
		computer_win +=1
		return True

	else:
		return False
def move_logic(no_1,no_2,no_3):
	global state_variable
	# print(state_variable)
	if state_variable < 1:
		if board_map[no_1] == 'X' and board_map[no_2] == 'X' and is_available(no_3) == True:
			computer_game_input(no_3)
			state_variable +=1
	else: 
		pass

def move_logic_2(no_1,no_2):
	global state_variable
	# print(state_variable)
	if state_variable < 1:
		if board_map[no_1] == 'X' and is_available(no_2) == True:
			computer_game_input(no_2)
			state_variable +=1
	else: 
		pass

#checking if the board is full
def is_board_full(b):
	i = 0
	#try:
	for k,v in b.items():
		if type(v) == int:
			i+=1
			#print("OK")
	# except KeyError:
	#print("i is {}".format(i))
	if i > 0:
		# print("Board is empty")
		return False

	elif i == 0:
		return True

#computer play
def computer_move():
	#total possible ways of winning include 3 vertical, 3 horizontal, 2 diagonal
	#the goal is to block each attempt
	# if state_number == 0:
	# 	move = random.randint(1,9)
	# 	computer_game_input(move)

	#shitty linear logic tyme
		#vertical all combos
	global state_variable
	state_variable = 0
	print("Now is the computer's turn douchebag")
	print("\n")

	#horizontal 
	move_logic(1,4,7)
	move_logic(1,7,2)
	move_logic(1,7,4)
	move_logic(4,7,1)
	move_logic(2,5,8)
	move_logic(5,8,2)
	move_logic(3,6,9)
	move_logic(6,9,3)
	#vertical
	move_logic(1,2,3)
	move_logic(1,3,2)
	move_logic(2,3,1)
	move_logic(4,5,6)
	move_logic(5,6,4)
	move_logic(7,8,9)
	move_logic(8,9,7)
	#diagonal
	move_logic(3,5,7)
	move_logic(7,5,3)
	move_logic(1,5,9)
	move_logic(5,9,1)
	#additional
	move_logic(1,9,5)
	move_logic(3,7,5)
	#duo_combos
	move_logic_2(2,8)
	move_logic_2(1,9)
	move_logic_2(3,9)
	move_logic_2(1,7)
	move_logic_2(1,3)
	move_logic_2(3,1)
	move_logic_2(3,7)
	move_logic_2(1,4)
	move_logic_2(7,4)
	move_logic_2(3,5)
	move_logic_2(5,3)
	# print(state_variable)

	if state_variable == 0:
	 	choose_random()

	make_board()
	
def who_won():

	return (any([player_won(1,4,7),player_won(2,5,8),player_won(3,6,9),player_won(1,2,3),player_won(4,5,6),player_won(7,8,9),player_won(1,5,9),player_won(3,5,7),computer_won(1,4,7),computer_won(2,5,8),computer_won(3,6,9),computer_won(1,2,3),computer_won(4,5,6),computer_won(7,8,9),computer_won(1,5,9),computer_won(3,5,7)]))



	
